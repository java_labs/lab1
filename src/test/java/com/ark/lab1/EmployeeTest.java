/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab1;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ahmed
 */
public class EmployeeTest {

    @Test
    public void givenNormalCase_whenCalculateTotalSalary_thenWillCalculated() {
        //Given
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Employee1", 1500));
        employees.add(new Employee("Employee2", 2000));
        employees.add(new Employee("Employee3", 3000));
        //When
        double totalSalary = Calculations.getTotalSalary(employees);
        //Then
        assertTrue("Total salary is not equal 6500", totalSalary == 6500);
    }

    
}
