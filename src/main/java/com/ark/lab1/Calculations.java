/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ark.lab1;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author ahmed
 */
public class Calculations {
    static double getTotalSalary(List<Employee> employees) {
        double totalSalary=0.0;
        for(Employee employee : employees){
            totalSalary+=employee.getSalary();
        }
        return totalSalary;
    }
}
